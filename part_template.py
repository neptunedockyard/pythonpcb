# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 15:54:11 2019

@author: mjaworski
"""

import uuid

class TestPart:
    
    pinCnt = 0
    pins = []
    footprint = []
    package = []
    
    design = [pins, footprint, package]
    
    def __init__(self, pinCount):
        self.pinCnt = pinCount

    #generate the ports using the given names
    def genPorts(self, portNames):
        for i in range(0, self.pinCnt):
            pinNum = i
            string = 'P%d'%(i,)
            name = portNames[i]
            pinID = str(uuid.uuid4())
            self.pins.append([pinNum, string, name, pinID, []])
            
    #add a new connection to the list
    def addConn(self, pin, conn):
        for i in range(0, len(conn)):
            self.pins[pin][4].append(conn[i])
        
    #overwrite the connection with a new one
    def setConn(self, pin, conn, newconn):
        self.pins[pin][4][conn] = newconn
        
    #remove the chosen connection
    def popConn(self, pin, conn):
        self.pins[pin][4].pop(conn)
            
x = TestPart(4)
x.genPorts(['Vcc', 'NC', 'NC', 'Gnd'])

x.addConn(0, ['18', '1', '7'])
print(x.pins)

x.setConn(0, 1, '7')
print(x.pins)

x.popConn(0, 2)
print(x.pins)
