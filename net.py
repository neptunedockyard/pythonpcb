# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 10:47:10 2019

@author: mjaworski
"""

import uuid
import drawSvg as draw

class netObj:
    
    # list of nets attached to this net object, add uuid's here
    netList = []
    # list of names of nets attached, should be same order as above
    netListNames = []
    # netlist name of this object
    netListName = ''
    # uuid holder for this net
    netListUUID = 0
    
    def __init__(self, netListName):
        self.netListName = netListName
        self.netListUUID = str(uuid.uuid4())
        print('Instantiating net object: ', self.netListName, ' with UUID: ', self.netListUUID)
        
    def addNet(self, netChild):
        self.netList.append(netChild.netChildUUID)
        self.netListNames.append(netChild.netChildName)
        print('Adding ', netChild.netChildName, ' to netList: ', self.netListName)
        
    def delNet(self, netChild):
        index = self.netList.index(netChild.netChildUUID)
        self.netList.pop(index)
        self.netListNames.pop(index)
        print('Popping ', netChild.netChildName, ' with UUID: ', netChild.netChildUUID, ' from netList: ', self.netListName)
        
class netChild:

    # name of this net
    netChildName = ''
    # uuid of this net
    netChildUUID = 0

    def __init__(self, netChildName):
        self.netChildName = netChildName
        self.netChildUUID = str(uuid.uuid4())
        print('Instantiating net child: ', self.netChildName, ' with UUID: ', self.netChildUUID)

    def getName(self):
        return self.netChildName
    
    def getUUID(self):
        return self.netChildUUID
    
class partObj:
    
    pinNumber = []
    pinNames = []
    child = 0
    wdt = 0
    hgt = 0
    dim = [wdt, hgt]
    left = 0
    right = 0
    top = 0
    bottom = 0
    layout = 0
    d = 0
    
    def __init__(self, partName):
        print('Instantiating part: ', partName)
        self.child = netChild(partName)
        print(self.child.netChildName)
        self.d = draw.Drawing(200, 100, origin='center')
        
    def addPin(self, pinList):
        print('Adding pin list, ', pinList, ' to part')
        for i in range(0, len(pinList)):
            self.pinNumber.append(i)
            self.pinNames.append(pinList[i])
            
    def addDim(self, wdt, hgt):
        print('Adding width: ', wdt, ' and height: ', hgt)
        self.wdt = wdt
        self.hgt = hgt
        
    def defLayout(self, left, right, top, bottom):
        print('Defining layout')
        self.left = left
        self.right = right
        self.top = top
        self.bottom = bottom
        self.layout = [left, right, top, bottom]
        print(self.layout)
        return self.layout
        
    def drawLayout(self):
        print('Drawing layout')
        self.d.setPixelScale(2)
        self.d.savePng('example.png')
        self.d.rasterize()
        print('Drawn')
            
    def genPart(self):
        print('Generating symbol for part')
        
    def drawRect(self, wdt, hgt, pins, layout):
        print('Drawing part')
        self.d.append(draw.Lines(0,0,
                                 0,6*layout[0],
                                 5*layout[2],6*layout[0],
                                 5*layout[2],0,
                                 0,0,
                                 close=True,
                                 fill='white',
                                 stroke='black'))
        
        #left side
        for i in range(1,layout[0]+1):
            self.d.append(draw.Lines(0,((6/3)*layout[0]*i),
                                     -6,((6/3)*layout[0]*i),
                                     close=False,
                                     stroke='black'))
        #right side
        for i in range(1,layout[1]+1):
            self.d.append(draw.Lines(5*layout[2],((6/3)*layout[0]*i),
                                     6+5*layout[2],((6/3)*layout[0]*i),
                                     close=False,
                                     stroke='black'))
        #top side
        for i in range(1,layout[2]+1):
            self.d.append(draw.Lines(((6/3)*layout[0]*i),0,
                                     ((6/3)*layout[0]*i),-6,
                                     close=False,
                                     stroke='black'))
        #bottom side
        for i in range(1,layout[3]+1):
            self.d.append(draw.Lines(((6/3)*layout[0]*i),6*layout[0],
                                     ((6/3)*layout[0]*i),6*layout[0]+6,
                                     close=False,
                                     stroke='black'))
        

        
    def translateRect(self, hor, ver, obj):
        print('Translating obj: ', obj, ' x: ', hor, ' y: ', ver)

def TEST1():
    c = netChild('u4')
    d = netChild('t7')
    f = netChild('h1')
    x = netObj('test')
    x.addNet(c)
    x.addNet(d)
    x.addNet(f)
    
    print('--------------------------------------------------------')
    print('TEST 1')
    print(x.netList)
    print(x.netListNames)

def TEST2():
    x = netObj('test')
    c = netChild('u4')
    d = netChild('t7')
    f = netChild('h1')
    x.addNet(c)
    x.addNet(d)
    x.addNet(f)
    x.delNet(c)
    print('--------------------------------------------------------')
    print('TEST 2')
    print(x.netList)
    print(x.netListNames)

def TEST3():
    print('--------------------------------------------------------')
    print('TEST 3')
    x = partObj('stm32f103')
    print(x.child.netChildName)
    print(x.child.netChildUUID)
    layout = x.defLayout(2,2,4,4)
    
    x.drawRect(10, 20, 8, layout)
    x.drawLayout()
    
    gnd = netObj('GND')
    gnd.addNet(x.child)
    
    print(gnd.netList)
    
    x.addPin(['Vcc', 'Gnd', 'NC'])
    
    print(x.pinNames)
    print(x.pinNumber)
    
TEST1()
TEST2()
TEST3()
