# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import PySimpleGUI as sg

# ------ Menu Definition ------ #
menu_def = [['File', ['New Project', 'Open Project', 'Close All Projects', '---', 'Quit']],
            ['Extras', ['Rescan Library', 'Open Library Manager', '---', 'Workspace Management']],
            ['Help', ['About']]
            ]
# ------ Interface Definition ------ #
button_def = [sg.Button('New Project'), sg.Button('Open Project'), sg.Button('Library Manager')]
# ------ Right click menu Definition ------ #
rc_menu = ['&Right', ['Right', '!&Click', '!&Menu']]

layout = [
        [sg.Menu(menu_def)],
        [sg.Listbox(values=['L1', 'L2', 'L3'], size=(800,6))],
        [sg.Text('Recent Projects', size=(60,1)), sg.Text('Favorite Projects', size=(60,1))],
        [sg.Multiline('Project Details\r\nTest', size=(65,5), background_color='lightgray'), sg.Multiline('Fav projects\r\nTest', size=(65,5), background_color='lightgray')],
        [sg.Multiline('License', size=(140,5))],
        [sg.Listbox(values=['L1', 'L2', 'L3'], size=(65,6)), sg.Listbox(values=['L1', 'L2', 'L3'], size=(65,6))],
        [sg.Button('New Project'), sg.Button('Open Project'), sg.Button('Library Manager')],
]

main_window = sg.Window('pyPCB', auto_size_text=True, default_element_size=(40,1), size=(1000,500), right_click_menu=rc_menu).Layout(layout)

# ------ Event checking ------ #
while True:
    event, values = main_window.Read()
    if event is None or event == 'Exit':
        break
    if event == 'Quit':
        print('Quit')
    if event == 'About':
        sg.Popup('About', 'Alpha V0.01')
    print(event, values)
    
main_window.Close()
